import spotify from "./Spotify";
import renderAlbums from "./AlbumList";

const albumList = document.getElementById("album-list");
const searchInput = document.getElementById("search-input");
const searchForm = document.getElementById("search-form");
let searchDelay = null;

function makeRequest(value) {
  spotify.search
    .albums(value)
    .then(data => renderAlbums(data.albums.items, albumList));
}

export default function searchEnterTrigger(params) {
  searchInput.addEventListener("keypress", e => {
    clearTimeout(searchDelay);
    if (searchInput.value.length < 2) return;
    searchDelay = setTimeout(() => makeRequest(searchInput.value), 500);
  });
  searchForm.addEventListener("submit", e => {
    e.preventDefault();
    makeRequest(searchInput.value);
  });
}
