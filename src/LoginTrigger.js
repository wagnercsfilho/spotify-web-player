import spotify from "./Spotify";

var stateKey = "spotify_auth_state";

function generateRandomString(length) {
  var text = "";
  var possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

function getHashParams() {
  var hashParams = {};
  var e,
    r = /([^&;=]+)=?([^&;]*)/g,
    q = window.location.hash.substring(1);
  while ((e = r.exec(q))) {
    hashParams[e[1]] = decodeURIComponent(e[2]);
  }
  return hashParams;
}

function makeLogin() {
  var client_id = "820b5668ec35472bb8968676cd57464c"; // Your client id
  var redirect_uri = "http://localhost:8080"; // Your redirect uri
  var state = generateRandomString(16);
  localStorage.setItem(stateKey, state);
  var scope = "user-read-private user-read-email";

  var url = "https://accounts.spotify.com/authorize";
  url += "?response_type=token";
  url += "&client_id=" + encodeURIComponent(client_id);
  url += "&scope=" + encodeURIComponent(scope);
  url += "&redirect_uri=" + encodeURIComponent(redirect_uri);
  url += "&state=" + encodeURIComponent(state);

  window.location = url;
}

function checkLoginStateRedirect(params) {
  var params = getHashParams();
  var access_token = params.access_token,
    state = params.state,
    storedState = localStorage.getItem(stateKey);

  if (access_token && (state == null || state !== storedState)) {
    alert("There was an error during the authentication");
  } else {
    localStorage.removeItem(stateKey);
    if (access_token) {
      fetch("https://api.spotify.com/v1/me", {
        headers: {
          Authorization: "Bearer " + access_token
        }
      })
        .then(response => response.json())
        .then(response => {
          spotify.token = access_token;
          localStorage.setItem(
            "@spotify/currentUser",
            JSON.stringify(response)
          );
          localStorage.setItem("@spotify/accessToken", access_token);
          document.getElementById("auth-modal").classList.remove("active");
          console.log(response);
        });
    }
  }
}

export default function loginTrigger() {
  const loginButton = document.getElementById("login-button");
  loginButton.addEventListener("click", makeLogin);
  checkLoginStateRedirect();
}
