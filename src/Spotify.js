import SpotifyWrapper from "spotify-wrapper";

const spotify = new SpotifyWrapper({
  token: localStorage.getItem("@spotify/accessToken")
});

export default spotify;
