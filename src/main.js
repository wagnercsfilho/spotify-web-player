import searchEnterTrigger from "./SearchTrigger";
import selectAlbumTrigger from "./SelectAlbumTrigger";
import playListTrigger from "./PlaylistTrigger";
import login from "./Login";

searchEnterTrigger();
selectAlbumTrigger();
playListTrigger();

login();
